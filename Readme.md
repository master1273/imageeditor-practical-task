The task is to developer an iOS application with the following features.

1.	On application start a user should see the main view with one big button in the center of the screen named “Load Image"
2.	When user presses the button he should be able to either  load an image from library or take a photo.
3.	When image is loaded - switch to edit view and show full screen image.
4.	Back button on edit view should take user back to main view with load button.
5.	Can zoom in/out and pan the loaded image in the main view.
6.	Add Invert button to the lower right corner of edit view. When a user presses it - invert image colors*.
7.	Add Select Color button to the lower right corner of edit view next to the Invert button. When it is pressed - show simple color picker popup. A 3rd party component can be used for color picker, prefer using it from cocoapods (http://cocoapods.org/). Some view with several predefined colors also is fine.
8.	User should be able to draw on top of the loaded image by finger. Color selected via color picker (see above) is used for drawing. Stoke thickness can be hardcoded.
9.	There should be Share button on the top right corner of the edit view. By pressing it user should be able to either save current image (with drawn strokes and possibly with inverted colors) to a library or email it.
10.	It should be iPad only application. It should work in the emulator.
11.	Support iOS 7 and later.
12.	Should work in both orientations correctly.
