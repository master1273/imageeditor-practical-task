//
//  ViewController.swift
//  ImageEditor
//
//  Created by Admin on 9/14/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    private var selectedImage: UIImage? = nil

    @IBAction func importImage(_ sender: Any) {
        let alert = UIAlertController(title: "Select Image", message: "", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "From Camera", style: .default) { (_) in
            self.selectImage(fromCamera: true)
        }
        
        let action2 = UIAlertAction(title: "From Gallery", style: .default) { (_) in
            self.selectImage(fromCamera: false)
        }
        
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImage = image
            self.performSegue(withIdentifier: "editor", sender: nil)
        } else {
            //Error
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    private func selectImage(fromCamera: Bool){
        let image = UIImagePickerController()
        image.delegate = self
        
        if (fromCamera){
            image.sourceType = UIImagePickerControllerSourceType.camera
        } else {
            image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        image.allowsEditing = false
        
        self.present(image, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detail: EditorViewController = segue.destination as! EditorViewController
        detail.setImage(image: selectedImage!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

