//
//  UIDrawView.swift
//  ImageEditor
//
//  Created by Admin on 9/14/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

open class UIDrawView: UIImageView {
    
    /// Used to keep track of the original image
    internal var originalImage : UIImage?
    
    /// Used to keep track of all the strokes
    internal var stack: [Stroke] = []
    
    /// Used to keep track of the current StrokeSettings
    fileprivate var settings = StrokeSettings()
    
    internal var drawEnabled = true
    private var inverted = false
    
    private var drawRect: CGRect?
    
    /// Exports the current drawing
    open func exportDrawing() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        self.image?.draw(in: frame)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    /// Clears the drawing
    @objc open func clearDrawing() {
        stack = []
        redrawStack()
    }
    
    /// Sets the brush's color
    open func setColor(_ color: UIColor?) {
        if color == nil {
            settings.color = nil
        } else {
            settings.color = CIColor(color: color!)
        }
    }
    
    /// Sets the brush's width
    open func setWidth(_ width: CGFloat) {
        settings.width = width
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        redrawStack()
    }
}

// MARK: - Touch Actions
extension UIDrawView {
    
    /// Triggered when touches begin
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if (drawEnabled) {
                
                let stroke = Stroke(points: [touch.location(in: self)], settings: settings)
                stack.append(stroke)
                
            }
        }
    }
    
    /// Triggered when touches move
    override open func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if (drawEnabled) {
                let stroke = stack.last!
                let lastPoint = stroke.points.last
                let currentPoint = touch.location(in: self)
                drawLineWithContext(fromPoint: lastPoint!, toPoint: currentPoint, properties: stroke.settings)
                stroke.points.append(currentPoint)
            }
        }
    }
    
    /// Triggered whenever touches end, resulting in a newly created Stroke
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (drawEnabled) {
            let stroke = stack.last!
            if stroke.points.count == 1 {
                let lastPoint = stroke.points.last!
                drawLineWithContext(fromPoint: lastPoint, toPoint: lastPoint, properties: stroke.settings)
            }
        }
    }
}

// MARK: - Drawing
fileprivate extension UIDrawView {
    
    /// Begins the image context
    func beginImageContext() {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, UIScreen.main.scale)
    }
    
    /// Ends image context and sets UIImage to what was on the context
    func endImageContext() {
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    /// Draws the current image for context
    func drawCurrentImage() {
        image?.draw(in: bounds)
    }
    
    /// Clears view, then draws stack
    func redrawStack() {
        beginImageContext()
        drawRect = getDrawRect(image: originalImage)
        originalImage?.draw(in: drawRect!)
        for stroke in stack {
            drawStroke(stroke)
        }
        endImageContext()
    }
    
    private func getDrawRect(image: UIImage?) -> CGRect?{
        if (image != nil) {
            
            let bw = bounds.width
            let bh = bounds.height
            
            let pw = image!.size.width
            let ph = image!.size.height
            
            let pa = pw / ph
            
            var rectToDraw: CGRect
            
            if (pw > ph){
                rectToDraw = CGRect(x: 0, y: bh / 2 - bw / pa / 2, width: bw, height: bw / pa)
            } else {
                rectToDraw = CGRect(x: bw / 2 - bh * pa / 2, y: 0, width: bh * pa, height: bh)
            }
            
            return rectToDraw
        }
        return nil
    }
    
    
    /// Draws a single Stroke
    func drawStroke(_ stroke: Stroke) {
        let properties = stroke.settings
        let points = stroke.points
        
        if points.count == 1 {
            let point = points[0]
            drawLine(fromPoint: point, toPoint: point, properties: properties)
        }
        
        for i in stride(from: 1, to: points.count, by: 1) {
            let point0 = points[i - 1]
            let point1 = points[i]
            drawLine(fromPoint: point0, toPoint: point1, properties: properties)
        }
    }
    
    /// Draws a single Stroke (begins/ends context
    func drawStrokeWithContext(_ stroke: Stroke) {
        beginImageContext()
        drawCurrentImage()
        drawStroke(stroke)
        endImageContext()
    }
    
    /// Draws a line between two points
    func drawLine(fromPoint: CGPoint, toPoint: CGPoint, properties: StrokeSettings) {
        let context = UIGraphicsGetCurrentContext()
        context!.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        context!.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
        
        context!.setLineCap(CGLineCap.round)
        context!.setLineWidth(properties.width)
        
        let color = properties.color
        
        if color != nil {
            if (!inverted){
                context!.setStrokeColor(red: properties.color!.red,
                                        green: properties.color!.green,
                                        blue: properties.color!.blue,
                                        alpha: properties.color!.alpha)
            } else {
                context!.setStrokeColor(red: 255 - properties.color!.red,
                                        green: 255 - properties.color!.green,
                                        blue: 255 - properties.color!.blue,
                                        alpha: properties.color!.alpha)
            }
            context!.setBlendMode(CGBlendMode.normal)
        } else {
            context!.setBlendMode(CGBlendMode.clear)
        }
        
        context!.strokePath()
    }
    
    /// Draws a line between two points (begins/ends context)
    func drawLineWithContext(fromPoint: CGPoint, toPoint: CGPoint, properties: StrokeSettings) {
        beginImageContext()
        drawCurrentImage()
        drawLine(fromPoint: fromPoint, toPoint: toPoint, properties: properties)
        endImageContext()
    }
    
}

extension UIDrawView {
    
    /// Sets the input image as the original image. This image is under the strokes
    public func setOriginalImage(image: UIImage?){
        originalImage = image
        drawRect = getDrawRect(image: originalImage)
        redrawStack()
    }
    
    public func setDrawEnabled(drawEnabled : Bool){
        self.drawEnabled = drawEnabled
    }
    
    func invert(){
        if (originalImage != nil) {
            let image = CIImage(cgImage: originalImage!.cgImage!)
            if let filter = CIFilter(name: "CIColorInvert") {
                filter.setDefaults()
                filter.setValue(image, forKey: kCIInputImageKey)
                
                let context = CIContext(options: nil)
                let imageRef = context.createCGImage(filter.outputImage!, from: image.extent)
                self.originalImage = UIImage(cgImage: imageRef!)
            }
        }
        inverted = !inverted
        redrawStack()
    }
}
