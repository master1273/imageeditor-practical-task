//
//  EditorViewController.swift
//  ImageEditor
//
//  Created by Admin on 9/14/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class EditorViewController: UIViewController, UIScrollViewDelegate {
    
    private var image: UIImage? = nil
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var drawView: UIDrawView!
    @IBOutlet weak var drawCheck: UISwitch!
    @IBOutlet weak var colorPickerFrame: UIView!
    
    private var draw: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareImage))
        self.navigationItem.rightBarButtonItem = shareButton
        

        drawView.setOriginalImage(image: image)
        
        setupDrag()
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        let colorPicker = ColorPickerView(frame: colorPickerFrame.bounds)
        colorPicker.didChangeColor = { color in
            self.drawView.setColor(color)
        }
        colorPickerFrame.addSubview(colorPicker)
        
        drawView.setColor(colorPicker.getCurrentColor())
        
    }
    
    @objc func shareImage(){
        // image to share
        let image = drawView.exportDrawing()
        
        // set up activity view controller
        let imageToShare = [ image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func setupDrag(){
        draw = drawCheck.isOn
        drawView.setDrawEnabled(drawEnabled: draw)
        
        scrollView.pinchGestureRecognizer?.isEnabled = !draw
        scrollView.panGestureRecognizer.isEnabled = !draw
    }
    
    func setImage(image: UIImage){
        self.image = image
        if (isViewLoaded){
            drawView.setOriginalImage(image: image)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return drawView
    }
    
    @IBAction func onDrawChecked(_ sender: Any) {
        draw = drawCheck.isOn
        setupDrag()
    }
    
    @IBAction func onInvertClick(_ sender: Any) {
        drawView.invert()
    }
}
